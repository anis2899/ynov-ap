

# This file is used for convenience of local development.
# DO NOT STORE YOUR CREDENTIALS INTO GIT
export POSTGRES_USERNAME=postgres_user
export POSTGRES_PASSWORD=password123
export POSTGRES_HOST=postgres
export POSTGRES_DB=db_pipeline
export AWS_BUCKET=bucket-pipeline_2899
export AWS_REGION=us-west-3
export AWS_PROFILE=default
export JWT_SECRET=testing
export URL=http://localhost:8100